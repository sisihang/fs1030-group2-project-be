import dotenv from 'dotenv';
dotenv.config()

import express from 'express'
import cors from 'cors'
import patientMedication from './src/patientMedication'
import patientAllergy from './src/patientAllergy'
import patientImmunization from './src/patientImmunization'
import revisionHistory from './src/revisionHistory'
import radiology from './src/radiology'
import billing from './src/billing'
import medicalHistory from './src/medicalHistory'
import patients from './src/patientRouter'
import province from './src/province'
import gender from './src/gender'
import maritalStatus from './src/maritalStatus'
import preferredLanguage from './src/preferredLanguage'
import race from './src/race'
// import patientContact from './src/patientContact--toremove'
import patientLaboratory from './src/labRouter'
import patientNotes from './src/chartEntriesRouter' 
import patientEmergency from './src/patientEmergency'

import demographic from './src/demographicRouter'
// import lookup from './src/lookupRouter'



const bodyParser = require('body-parser');






const app = express()
const PORT = process.env.PORT || 3001
const mysql = require('mysql');
app.use(express.json())
app.use(cors())

app.use(function(req, res, next){
    global.connection = mysql.createConnection({
        host     : 'localhost',
        user     : 'root',
        password : '1234567',
        database : 'emr',
        dateStrings: 'date'
    });
    connection.connect(function (err) {
        if (err) {
          console.error("error connecting: " + err.stack);
          return;
        }
        console.log("Database connected");
      });
    next();
});

//test route to see if route is working//
//app.get("/api/testadmin", (req, res) => {
    //connection.query("SELECT * FROM testadmin", function (error, results, fields) {
     // if (error) throw error;
     // return res.status(200).send(results);
  //  });
  //});  
  


//app.get("/", (req, res) => {
    //return res.send ("Hello World!")
//});




app.use('/patient_medication', patientMedication)
app.use('/patient_allergy', patientAllergy)
app.use('/patient_immunization', patientImmunization)
app.use('/revision_history', revisionHistory)
app.use('/radiology', radiology)
app.use('/billing', billing)
app.use('/medical_history', medicalHistory)
app.use('/api/patients', patients)
// app.use('/api/patientContact', patientContact)
app.use('/api/province', province)
app.use('/api/gender', gender)
app.use('/api/maritalStatus', maritalStatus)
app.use('/api/preferredLanguage', preferredLanguage)
app.use('/api/race', race)
app.use('/api/patient_laboratory', patientLaboratory)
app.use('/api/patient_notes', patientNotes)
app.use('/api/patient_emergency', patientEmergency)
app.use('/api/patient_demographics', demographic)




// parse request data content type application/x-www-form-rulencoded
app.use(bodyParser.urlencoded({extended: false}));

// parse request data content type application/json
app.use(bodyParser.json());

// define root route

app.get("/api/patient", (req, res) => {
  connection.query(
    "SELECT * FROM PATIENT",
    function (error, results, fields) {
      if (error) throw error;
       return res.status(200).send(results);
    }
  );
});


// import patient routes
const patientRoutes = require('./src/routes/patient.route');

// create patient routes
app.use('/api/v1/patient', patientRoutes);




// define root route careprovider//

app.get("/api/careprovider", (req, res) => {
  connection.query(
    "SELECT * FROM CAREPROVIDER",
    function (error, results, fields) {
      if (error) throw error;
       return res.status(200).send(results);
    }
  );
});


// import careprovider routes
const careproviderRoutes = require('./src/routes/careprovider.route');

// create careprovider routes
app.use('/api/v1/careprovider', careproviderRoutes);



app.post('/auth', (req, res) => {
  const username = req.body.username
  const password = req.body.password

  if (username == "test" && password == "password") {
      const token = jwtGenerator.sign({username}, process.env.JWT_SECRET, {expiresIn: '1h'})
      return res.send({token})
  }
  return res.status(401).send({error: "incorrect username\password"})
})


app.listen(PORT, () => {
    console.log(` Express Server started at http://localhost:${PORT}`)
})

// LOGIN//

//app.post('/auth', (req, res) => {
 // const username = req.body.username
  //const password = req.body.password

 // if (username == "test" && password == "password") {
  //    const token = jwtGenerator.sign({username}, process.env.JWT_SECRET, {expiresIn: '1h'})
  //    return res.send({token})
//  }
 // return res.status(401).send({error: "incorrect username\password"})
//})

