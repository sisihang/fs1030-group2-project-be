import express from "express"; 

const router = express.Router();

// List all patients
router.get("/", (req, res) => {
    const sqlSelect = `SELECT patient.patient_id, healthcard_no, healthcard_ver, first_name, middle_name, last_name, address_line1, address_line2, city, province_code, postal_code, home_phone, cell_phone, email, family_doctor_code, employer_name, work_phone, occupation, dob, gender_code, race_id, marital_status_code, pref_language_id
    FROM patient
    LEFT OUTER JOIN demographic
    ON patient.patient_id = demographic.patient_id`;
    connection.query(sqlSelect, (error, results) => {
      if (error) throw error;
      return res.status(200).send(results);
    });
  });


  
// Pull one patient
router.get("/:id", (req, res) => {    
  const sqlSelect =     
  `SELECT patient.patient_id, healthcard_no, healthcard_ver, first_name, middle_name, last_name,
  address_line1, address_line2, city, patient.province_code, province.province_name, postal_code, 
  home_phone, cell_phone, patient.email, patient.family_doctor_code,
  demographic.employer_name, demographic.work_phone, demographic.occupation, demographic.dob,
  DATE_FORMAT(FROM_DAYS(DATEDIFF(now(), dob)), '%Y')+0 AS age,
  demographic.gender_code, gender.gender_description,
  demographic.race_id, race.race_name,
  demographic.marital_status_code, marital_status_desc,
  demographic.pref_language_id, preferredLanguage.pref_language_name,
  careProvider.care_provider_fname, careProvider.care_provider_mname, careProvider.care_provider_lname
  FROM patient
  LEFT OUTER JOIN careProvider
  ON patient.family_doctor_code = careProvider.care_provider_id
  LEFT OUTER JOIN province
  ON patient.province_code = province.province_code
  LEFT OUTER JOIN demographic
  ON patient.patient_id = demographic.patient_id
  LEFT OUTER JOIN gender
  ON demographic.gender_code = gender.gender_code
  LEFT OUTER JOIN race
  ON demographic.race_id = race.race_id
  LEFT OUTER JOIN maritalStatus
  ON demographic.marital_status_code = maritalStatus.marital_status_code
  LEFT OUTER JOIN preferredLanguage
  ON demographic.pref_language_id = preferredLanguage.pref_language_id
  WHERE patient.patient_id = ${req.params.id}`  


    connection.query(sqlSelect,    
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });



 // Update patient main information
 router.put("/:id", (req, res) => {
  const { 
    first_name,
    middle_name,
    last_name,
    address_line1,
    address_line2,
    city,
    province_code,
    postal_code,
    home_phone,
    cell_phone,
    email
  } = req.body;
  const sqlUpdate = 
  `UPDATE patient
  SET first_name = "${first_name}",
  middle_name = "${middle_name}",
  last_name = "${last_name}",
  address_line1 = "${address_line1}",
  address_line2 = "${address_line2}",
  city = "${city}",
  province_code = "${province_code}",
  postal_code = "${postal_code}",
  home_phone = "${home_phone}",
  cell_phone = "${cell_phone}",
  email = "${email}"
  WHERE patient_id = ${req.params.id}`

  connection.query(sqlUpdate,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});



export default router;
