const express = require('express');
const router = express.Router();

const careproviderController = require('../controllers/careprovider.controller');

// get all careprovider

router.get('/', careproviderController.getCareproviderList);

// get careprovider by ID


router.get('/:id', careproviderController.getCareproviderByID);


// get ID for Update 

router.get('/searchRecord/:first_name', careproviderController.getCareproviderByName);

// create new careprovider

router.post('/', careproviderController.createNewCareprovider);

// update careprovider

router.put('/:id', careproviderController.updateCareprovider);

// delete careprovider
router.delete('/:id', careproviderController.deleteCareprovider);

module.exports = router;