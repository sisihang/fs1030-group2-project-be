import express from "express";

const router = express.Router();

router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO patient_medication (medication_name, instruction, patient_id, care_provider_id) VALUES  (?, ?, ?, ?)",
    [
      req.body.medicationName,
      req.body.instruction,
      req.body.patientId,
      req.body.careProviderId,
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

router.delete("/:id", (req, res) => {
  connection.query(
    `DELETE FROM patient_medication WHERE medication_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.put("/:id", (req, res) => {
  const { medication_name, instruction } = req.body;
  connection.query(
    `UPDATE patient_medication SET medication_name="${medication_name}", instruction="${instruction}" WHERE medication_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.get("/:id", (req, res) => {
  connection.query(
    `SELECT medication_id, medication_name, instruction from patient_medication WHERE patient_id= ${req.params.id};`,
    function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

export default router;