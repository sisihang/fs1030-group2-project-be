import express from "express"; 
import { v4 as uuidv4 } from "uuid"; // use to auto generate id



// const lookupRouter = express.Router();

const router = express.Router();

// checks invalid properties/data -- need to update fields
const validatePatient = (req, res, next) => { // middleware    
    const patientValidProp = ["healthCard_No", 
                            "healthCard_Ver",
                            "firstName", 
                            "lastName",
                            "streetNo",
                            "streetName",
                            "City",
                            "Province",
                            "postalCode",
                            "homePhone"]; // required properties
    let errorMessages = validateData("patient", patientValidProp, req.body);
    if (errorMessages.length < 1) {
        next();
    } else {
        const errorMessageObj = {
            message: "Validation error",
            invalid: errorMessages,
        };
        return res.status(400).json(errorMessageObj); // return error in json format indicating invalid/missing properties/data
    };  
};

// Province
router.get("/api/province", (req, res) => {
    const sqlSelectAll = "SELECT * FROM province";
    connection.query(sqlSelectAll, (error, results) => {
      if (error) throw error;
      return res.status(200).send(results);
    });
  });


  
// Race
router.get("/api/race", (req, res) => {
    const sqlSelectAll = "SELECT * FROM race";
    connection.query(sqlSelectAll, (error, results) => {
      if (error) throw error;
      return res.status(200).send(results);
    });
  });


  // Gender
  router.get("/api/gender", (req, res) => {
  const sqlSelectAll = "SELECT * FROM gender";
  connection.query(sqlSelectAll, (error, results) => {
    if (error) throw error;
    return res.status(200).send(results);
  });
});

  // Marital Status
  router.get("/api/maritalstatus", (req, res) => {
    const sqlSelectAll = "SELECT * FROM maritalStatus";
    connection.query(sqlSelectAll, (error, results) => {
      if (error) throw error;
      return res.status(200).send(results);
    });
  });

    // Preferred Language
    router.get("/api/preferredlanguage", (req, res) => {
      const sqlSelectAll = "SELECT * FROM preferredLanguage";
      connection.query(sqlSelectAll, (error, results) => {
        if (error) throw error;
        return res.status(200).send(results);
      });
    });

export default router;




