import express from "express"; 

const router = express.Router();

// List patient's emergency contacts 
router.get("/:id", (req, res) => {
    const sqlSelect = `SELECT * FROM patientContact WHERE patient_id = ${req.params.id}`;
    connection.query(sqlSelect, (error, results) => {
      if (error) throw error;
      return res.status(200).send(results);
    });
  });


// Pull one emergency contact of a patient
router.get("/:id/:contactID", (req, res) => {
  const sqlSelect = 
  `SELECT * FROM patientContact WHERE patient_id = ${req.params.id} AND contact_id = ${req.params.contactID}`;
  connection.query(sqlSelect, (error, results) => {
    if (error) throw error;
    return res.status(200).send(results);
  });
});  


// Insert patient emergency contact
router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO patientContact (contact_fname, contact_lname, contact_phone, relationship, patient_id) VALUES (?, ?, ?, ?, ?)",
    [req.body.contact_fname, 
    req.body.contact_lname, 
    req.body.contact_phone, 
    req.body.relationship,       
    req.body.patient_id],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});


 // Update patient emergency contact
 router.put("/:id", (req, res) => {
  const { 
    contact_fname, 
    contact_lname, 
    contact_phone, 
    relationship,
    patient_id
  } = req.body;
  const sqlUpdate = 
  `UPDATE patientContact
  SET contact_fname = "${contact_fname}",
  contact_lname = "${contact_lname}",
  contact_phone = "${contact_phone}",
  relationship = "${relationship}"
  pateint_id = "${patient_id}"
  WHERE contact_id = ${req.params.id}`
  connection.query(sqlUpdate,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


// delete patient emergency contact
router.delete("/:id", (req, res) => {
  connection.query(
    `DELETE FROM patientContact WHERE contact_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

export default router;
