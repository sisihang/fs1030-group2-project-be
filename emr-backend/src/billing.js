import express from "express";

const router = express.Router();

router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO billing (care_provider_id, patient_id, service_rendered, service_date, billing_date, due_date, total_amount, payment, balance) VALUES  (?, ?, ?, ?, ?, ?, ?, ?, ?)",
    [
      
      req.body.careProviderId,
      req.body.patientId,
      req.body.serviceRendered,
      req.body.serviceDate,
      req.body.billingDate,
      req.body.dueDate,
      req.body.totalAmount,
      req.body.payment,
      req.body.balance,
      
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

router.delete("/:id", (req, res) => {
  connection.query(
    `DELETE FROM billing WHERE bill_invoice_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.put("/:id", (req, res) => {
  const { service_rendered, service_date, billing_date, due_date, total_amount, payment, balance } = req.body;
  connection.query(
    `UPDATE billing SET service_rendered="${service_rendered}", service_date="${service_date}", billing_date="${billing_date}", due_date="${due_date}", total_amount="${total_amount}", payment="${payment}", balance="${balance}" WHERE bill_invoice_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.get("/:id", (req, res) => {
  connection.query(
    `SELECT bill_invoice_id, care_provider_id, service_rendered, SUBSTRING_INDEX(billing.service_date, "T", 1) as service_date, SUBSTRING_INDEX(billing.billing_date, "T", 1) as billing_date,  SUBSTRING_INDEX(billing.due_date, "T", 1) as due_date, total_amount, payment, balance FROM billing WHERE patient_id= ${req.params.id};`,
    function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

router.get("/", (req, res) => {
  connection.query(
    "SELECT * FROM billing",
    function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});



export default router;