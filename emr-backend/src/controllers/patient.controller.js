
const PatientModel = require('../models/patient.model');

// get all patient list
exports.getPatientList = (req, res)=> {
    //console.log('here all patient list');
    PatientModel.getAllPatient((err, patient) =>{
        console.log('done');
        if(err)
        res.send(err);
        console.log('Patient', patient);
        res.send(patient)
    })
}

// get patient by Name for earch by Name 
exports.getPatientByName = (req, res)=>{
    //console.log('get patient by id');
    PatientModel.getPatientByName(req.params.first_name, (err, patient)=>{
        if(err)
        res.send(err);
        console.log('single patient data',patient);
        res.send(patient);
    })
}


// create new patient
exports.createNewPatient = (req, res) =>{
    const patientReqData = new PatientModel(req.body);
    console.log('patientReqData', patientReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        PatientModel.createPatient(patientReqData, (err, patient)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: ' Patient Created Successfully', data: patient.insertId})
        })
    }
}


// get patient by ID  for Update 
exports.getPatientByID = (req, res)=>{
    //console.log('get patient by id');
    PatientModel.getPatientByID(req.params.id, (err, patient)=>{
        if(err)
        res.send(err);
        console.log('single patient data',patient);
        // res.json({"first_name":""});
        res.send(JSON.stringify({ status: 200, error: null, response: patient }));
    })
}


// update patient
exports.updatePatient = (req, res)=>{
    const patientReqData = new PatientModel(req.body);
    console.log('patientReqData update', patientReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        PatientModel.updatePatient(req.params.id, patientReqData, (err, patient)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'Patient updated Successfully'})
        })
    }
}

// delete patient
exports.deletePatient = (req, res)=>{
    PatientModel.deletePatient(req.params.id, (err, patient)=>{
        if(err) {
          return res.send(err);
        }
        res.json({success:true, message: 'Patient deleted successully!'});
    })
} ;