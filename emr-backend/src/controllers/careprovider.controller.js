
const CareproviderModel = require('../models/careprovider.model');

// get all careprovider list

exports.getCareproviderList = (req, res)=> {
    //console.log('here all careprovider list');

    CareproviderModel.getAllCareprovider((err, careprovider) =>{
        console.log('done');
        if(err)
        res.send(err);
        console.log('Careprovider', careprovider);
        res.send(careprovider)
    })
}

// get careprovider by Name for earch by Name 

exports.getCareproviderByName = (req, res)=>{
    //console.log('get careprovider by id');
    CareproviderModel.getCareproviderByName(req.params.first_name, (err, careprovider)=>{
        if(err)
        res.send(err);
        console.log('single careprovider data',careprovider);
        res.send(careprovider);
    })
}


// create new careprovider


exports.createNewCareprovider = (req, res) =>{
    const careproviderReqData = new CareproviderModel(req.body);
    console.log('careproviderReqData', careproviderReqData);
    // check null
    
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        CareproviderModel.createCareprovider(careproviderReqData, (err, careprovider)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: ' Careprovider Created Successfully', data: careprovider.insertId})
        })
    }
}


// getcareproviderby ID  for Update 

exports.getCareproviderByID = (req, res)=>{
    //console.log('get careprovider by id');
    CareproviderModel.getCareproviderByID(req.params.id, (err, careprovider)=>{
        if(err)
        res.send(err);
        console.log('single careprovider data',careprovider);
        // res.json({"first_name":""});
        res.send(JSON.stringify({ status: 200, error: null, response: careprovider }));
    })
}


// update careprovider

exports.updateCareprovider = (req, res)=>{
    const careproviderReqData = new CareproviderModel(req.body);
    console.log('careproviderReqData update', careproviderReqData);
    // check null
    if(req.body.constructor === Object && Object.keys(req.body).length === 0){
        res.send(400).send({success: false, message: 'Please fill all fields'});
    }else{
        CareproviderModel.updateCareprovider(req.params.id, careproviderReqData, (err, careprovider)=>{
            if(err)
            res.send(err);
            res.json({status: true, message: 'Careprovider updated Successfully'})
        })
    }
}

// delete careprovider
exports.deleteCareprovider = (req, res)=>{
    CareproviderModel.deleteCareprovider(req.params.id, (err, careprovider)=>{
        if(err) {
          return res.send(err);
        }
        res.json({success:true, message: 'Careprovider deleted successully!'});
    })
} 