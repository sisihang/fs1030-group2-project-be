import express from "express";

const router = express.Router();

router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO medical_history ( patient_id, care_provider_id, medical_history_date, major_illness, surgical_procedure, comments) VALUES  (?, ?, ?, ?, ?, ?)",
    [
      req.body.patientId,
      req.body.careProviderId,
      req.body.medHistoryDate,
      req.body.majIllness,
      req.body.surgProcedure,
      req.body.comments,
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

router.delete("/:id", (req, res) => {
  connection.query(
    `DELETE FROM medical_history WHERE medical_history_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.put("/:id", (req, res) => {
  const { medical_history_date, major_illness, surgical_procedure, comments } = req.body;
  connection.query(
    `UPDATE medical_history SET medical_history_date="${medical_history_date}", major_illness="${major_illness}", surgical_procedure="${surgical_procedure}", comments="${comments}" WHERE medical_history_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.get("/", (req, res) => {
  connection.query(
    "SELECT * FROM medical_history",
    function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

router.get("/:id", (req, res) => {
  connection.query(
    `SELECT medical_history_id, care_provider_id, SUBSTRING_INDEX(medical_history.medical_history_date, "T", 1) as medical_history_date, major_illness, surgical_procedure, comments FROM medical_history WHERE patient_id=${req.params.id}`,
    function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

export default router;