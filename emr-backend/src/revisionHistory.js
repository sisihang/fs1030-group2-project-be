import express from "express";

const router = express.Router();

router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO change_history (column_name, revision_date, patient_id, care_provider_id) VALUES  (?, ?, ?, ?)",
    [
      req.body.stringUpdatedFields,
      req.body.dateUpdtaed,
      req.body.patientId,
      req.body.careProviderId,
      1,
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

router.get("/:id", (req, res) => {
  connection.query(
    `SELECT change_history.column_name, change_history.revision_date,CONCAT(careProvider.care_provider_fname, ' ', careProvider.care_provider_mname, ' ', careProvider.care_provider_lname) as careProvider_name FROM change_history LEFT JOIN careProvider ON change_history.care_provider_id = careProvider.care_provider_id WHERE patient_id = ${req.params.id};`,
    function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

export default router;