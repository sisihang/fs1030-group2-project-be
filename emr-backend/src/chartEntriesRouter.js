import express from "express"; 

const router = express.Router();

// Pull all notes of a patient
router.get("/:id", (req, res) => {
    const sqlSelectAll = 
    `SELECT chart_entry_id, chart_entry.patient_id, chart_entry_date, comments,
    chart_entry.care_provider_id, careProvider.care_provider_fname, careProvider.care_provider_mname, careProvider.care_provider_lname, comments
    FROM chart_entry
    LEFT OUTER JOIN careProvider
    ON chart_entry.care_provider_id = careProvider.care_provider_id
    WHERE patient_id = ${req.params.id}
    ORDER BY chart_entry_date DESC`;

    connection.query(sqlSelectAll, (error, results) => {
      if (error) throw error;
      return res.status(200).send(results);
    });
  });


  // Pull one note of a patient
router.get("/:id/:noteID", (req, res) => {
  const sqlSelectAll = 
  `SELECT chart_entry_id, chart_entry.patient_id, chart_entry_date, comments,
  chart_entry.care_provider_id, careProvider.care_provider_fname, careProvider.care_provider_mname, careProvider.care_provider_lname, comments
  FROM chart_entry
  LEFT OUTER JOIN careProvider
  ON chart_entry.care_provider_id = careProvider.care_provider_id
  WHERE patient_id = ${req.params.id} AND chart_entry_id = ${req.params.noteID}
  ORDER BY chart_entry_date DESC`;

  connection.query(sqlSelectAll, (error, results) => {
    if (error) throw error;
    return res.status(200).send(results);
  });
});



// Add note  
router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO chart_entry (chart_entry_date, comments, care_provider_id, patient_id) VALUES (?, ?, ?, ?)",
    [req.body.chart_entry_date, 
      req.body.comments, 
      req.body.care_provider_id,
      req.body.patient_id],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});


 // Update notes
 router.put("/:id", (req, res) => {
  const { 
    chart_entry_date,
    comments,
    patient_id,
    care_provider_id
  } = req.body;

  const sqlUpdate = 
  `UPDATE chart_entry
  SET chart_entry_date = "${chart_entry_date}",
  comments = "${comments}",
  patient_id = "${patient_id}",
  care_provider_id = "${care_provider_id}"
  WHERE chart_entry_id = "${req.params.id}"`;

  connection.query(sqlUpdate,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

export default router;