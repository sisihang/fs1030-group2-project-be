import express from "express";

const router = express.Router();

router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO patient_allergy (allergy_name, serverity, comments, patient_id, care_provider_id) VALUES  (?, ?, ?, ?, ?)",
    [
      req.body.allergyname,
      req.body.serverity,
      req.body.comments,
      req.body.patientId,
      req.body.careProviderId,
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

router.delete("/:id", (req, res) => {
  connection.query(
    `DELETE FROM patient_allergy WHERE allergy_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.put("/:id", (req, res) => {
  const { allergy_name, serverity, comments } = req.body;
  connection.query(
    `UPDATE patient_allergy SET allergy_name="${allergy_name}", serverity="${serverity}", comments="${comments}"  WHERE allergy_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.get("/:id", (req, res) => {
  connection.query(
    `SELECT allergy_id, allergy_name, serverity, comments from patient_allergy WHERE patient_id = ${req.params.id};`,
    function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

export default router;