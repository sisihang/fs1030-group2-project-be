import express from "express"; 

const router = express.Router();


// Update demographics 
router.put("/:id", (req, res) => {
  const { 
    employer_name, 
    work_phone, 
    occupation,
    dob, 
    patient_id, 
    gender_code, 
    pref_language_id, 
    race_id,
    marital_status_code
  } = req.body;
  const sqlUpdate = 
  `UPDATE demographic
  SET employer_name = "${employer_name}",
  work_phone = "${work_phone}",
  occupation = "${occupation}",
  dob = "${dob}",
  patient_id = "${patient_id}",
  gender_code = "${gender_code}",
  pref_language_id = "${pref_language_id}",
  race_id = "${race_id}",
  marital_status_code = "${marital_status_code}"
  WHERE demo_id = ${req.params.id}`;

  connection.query(sqlUpdate,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


export default router;
