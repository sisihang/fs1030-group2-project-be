import express from "express";

const router = express.Router();

router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO radiology (image_type, radio_image, patient_id, radio_date, radiographer, care_provider_id, clinic_name, comments) VALUES  (?, ?, ?, ?, ?, ?, ?, ?)",
    [
      req.body.imageType,
      req.body.radioImage,
      req.body.patientId,
      req.body.radioDate,
      req.body.radiographer,
      req.body.careProviderId,
      req.body.clinicName,
      req.body.comments
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

router.delete("/:id", (req, res) => {
  connection.query(
    `DELETE FROM radiology WHERE radiograph_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.put("/:id", (req, res) => {
  const { image_type, radio_image, radiographer, radio_date,  clinic_name, comments } = req.body;
  connection.query(
    `UPDATE radiology SET radio_image="${radio_image}", image_type="${image_type}", radiographer="${radiographer}", radio_date="${radio_date}", clinic_name="${clinic_name}", comments="${comments}" WHERE radiograph_id="${req.params.id}"`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.get("/", (req, res) => {
  connection.query(
    "SELECT * FROM radiology",
    function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

router.get("/:id", (req, res) => {
  connection.query(
    `SELECT radiograph_id, image_type, radio_image, SUBSTRING_INDEX(radiology.radio_date, "T", 1) as radio_date, radiographer, care_provider_id, clinic_name, comments FROM radiology WHERE patient_id=${req.params.id};`,
    function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

export default router;