import express from "express";

const router = express.Router();

// Preferred Language
router.get("/", (req, res) => {
  const sqlSelectAll = "SELECT * FROM preferredLanguage";
  connection.query(sqlSelectAll, (error, results) => {
    if (error) throw error;
    return res.status(200).send(results);
  });
});


export default router;