const dbConn = require('../../index');


class Careprovider {
    constructor(careprovider) {
        this.first_name = careprovider.fname;
        this.middle_name = careprovider.mname;
        this.last_name = careprovider.lname;
        this.email = careprovider.email;
        this.cspo_number =careprovider.csponumber;
        this.initial_setup =careprovider.initialsetup;
        this.careprovider_type_id = careprovider.careprovidertypeid;
 }



    
    // get all careprovider

    static getAllCareprovider(result) {

      connection.query('SELECT * FROM CAREPROVIDER', (err, res) => {
            if (err) {
                console.log('Error while fetching careprovider', err);
                result(null, err);
            } else {
                console.log('Careprovider fetched successfully');
                result(null, res);
            }
        });
    }
    // get careprovider by Name for Search Data by name 

    static getCareproviderByName(first_name, result) {
      connection.query('SELECT * FROM careprovider WHERE first_name=?', first_name, (err, res) => {
            if (err) {
                console.log('Error while fetching careprovider by id', err);
                result(null, err);
            } else {
                result(null, res);
            }
        });
    }
    // create new careprovider

    static createCareprovider(careproviderReqData, result) {
      connection.query('INSERT INTO careprovider SET ?', careproviderReqData, (err, res) => {
            if (err) {
                console.log('Error while inserting data');
                result(null, err);
            } else {
                console.log('Careprovider created successfully');
                result(null, res);
            }
        });
    }
    // get careprovider by ID for update

    static getCareproviderByID(id, result) {
      connection.query('SELECT * FROM careprovider WHERE id=?', id, (err, res) => {
            if (err) {
                console.log('Error while fetching careprovider by id', err);
                result(null, err);
            }

            else {
                result(null, res);
            }
        });
    }


    // update careprovider

    static updateCareprovider = (id, careproviderReqData, result) => {
      connection.query("UPDATE careprovider SET first_name=?,middle_name=?,last_name=?,email=?,cspo_number=?,initial_setup=?, careprovider_type_id=?  WHERE id = ?", [careproviderReqData.first_name, careproviderReqData.middle_name, careproviderReqData.last_name, careproviderReqData.email, careproviderReqData.cpso_number, careproviderReqData.initial_setup, careproviderReqData.careprovider_type_id, id], (err, res) => {
            if (err) {
                console.log('Error while updating the patient');
                result(null, err);
            } else {
                console.log("Patient updated successfully");
                result(null, res);
            }
        });
    }
    
    
    
    // delete careprovider


    static deleteCareprovider(id, result) {
      connection.query('DELETE FROM careprovider WHERE id=?', [id], (err, res) => {
            if (err) {
                console.log('Error while deleting the careprovider');
                result(null, err);
            } else {
                result(null, res);
            }
        });
      connection.query("UPDATE careprovider SET is_deleted=? WHERE id = ?", [1, id], (err, res) => {
            if (err) {
                console.log('Error while deleting the careprovider');
                result(null, err);
            } else {
                console.log("Careprovider deleted successfully");
                result(null, res);
            }
        });
    }
}









module.exports = Careprovider;