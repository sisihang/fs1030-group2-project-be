const dbConn = require('../../index');


class Patient {
    constructor(patient) {
        this.healthcard_no = patient.healthcardno;
        this.healthcard_ver = patient.healthcardver;
        this.first_name = patient.fname;
        this.middle_name = patient.mname;
        this.last_name = patient.lname;
        this.address_line1 = patient.addressline1;
        this.address_line2 = patient.addressline2;
        this.city = patient.city;
        this.province_code = patient.provincecode;
        this.postal_code = patient.postalcode;
        this.home_phone = patient.homephone;
        this.cell_phone = patient.cellphone;
        this.email = patient.email;
        this.family_doctor_code = patient.familydoctorcode;

    }


    
    // get all patient

    static getAllPatient(result) {

      connection.query('SELECT * FROM PATIENT', (err, res) => {
            if (err) {
                console.log('Error while fetching patient', err);
                result(null, err);
            } else {
                console.log('Patient fetched successfully');
                result(null, res);
            }
        });
    }
    // get patient by Name for Search Data by name 

    static getPatientByName(first_name, result) {
      connection.query('SELECT * FROM patient WHERE first_name=?', first_name, (err, res) => {
            if (err) {
                console.log('Error while fetching patient by id', err);
                result(null, err);
            } else {
                result(null, res);
            }
        });
    }
    // create new patient

    static createPatient(patientReqData, result) {
      connection.query('INSERT INTO patient SET ?', patientReqData, (err, res) => {
            if (err) {
                console.log('Error while inserting data');
                result(null, err);
            } else {
                console.log('Patient created successfully');
                result(null, res);
            }
        });
    }
    // get employee by ID for update

    static getPatientByID(id, result) {
      connection.query('SELECT * FROM patient WHERE id=?', id, (err, res) => {
            if (err) {
                console.log('Error while fetching patient by id', err);
                result(null, err);
            }

            else {
                result(null, res);
            }
        });
    }


    // update patient

    static updatePatient = (id, patientReqData, result) => {
      connection.query("UPDATE patient SET healthcard_no=?,healthcard_ver=?,first_name=?,middle_name=?,last_name=?,address_line1=?,address_line2=?,city=?,province_code=?,postal_code=?,home_phone=?,cell_phone=?,email=?,family_doctor_code=?  WHERE id = ?", [patientReqData.healthcard_no, patientReqData.healthcard_ver, patientReqData.first_name, patientReqData.middle_name, patientReqData.last_name, patientReqData.address_line1, patientReqData.address_line2, patientReqData.city, patientReqData.province_code, patientReqData.postal_code, patientReqData.home_phone, patientReqData.cell_phone, patientReqData.email, patientReqData.family_doctor_code, id], (err, res) => {
            if (err) {
                console.log('Error while updating the patient');
                result(null, err);
            } else {
                console.log("Patient updated successfully");
                result(null, res);
            }
        });
    }
    
    
    
    // delete patient


    static deletePatient(id, result) {
      connection.query('DELETE FROM patient WHERE id=?', [id], (err, res) => {
            if (err) {
                console.log('Error while deleting the patient');
                result(null, err);
            } else {
                result(null, res);
            }
        });
      connection.query("UPDATE patient SET is_deleted=? WHERE id = ?", [1, id], (err, res) => {
            if (err) {
                console.log('Error while deleting the patient');
                result(null, err);
            } else {
                console.log("Patient deleted successfully");
                result(null, res);
            }
        });
    }
}









module.exports = Patient;