import express from "express"; 

const router = express.Router();


// List all labs of one patient
router.get("/:id", (req, res) => {
    const sqlSelect = 
    `SELECT lab_id, lab_date, patient_lab.lab_code, labDescription.lab_desc,
    lab_order_by, patient_lab.lab_order_by, careProvider.care_provider_fname, careProvider.care_provider_mname, careProvider.care_provider_lname,
    technician,
    laboratory, result, comments, patient_id
    FROM patient_lab
    LEFT OUTER JOIN labDescription
    ON patient_lab.lab_code = labDescription.lab_code
    LEFT OUTER JOIN careProvider
    ON patient_lab.lab_order_by = careProvider.care_provider_id
    WHERE patient_lab.patient_id = ${req.params.id}`;

    connection.query(sqlSelect, (error, results) => {
      if (error) throw error;
      return res.status(200).send(results);
    });
  });


// Pull one lab of a patient
router.get("/:id/:labID", (req, res) => {
  const sqlSelect = 
  `SELECT lab_id, lab_date, patient_lab.lab_code, labDescription.lab_desc,
  lab_order_by, patient_lab.lab_order_by, careProvider.care_provider_fname, careProvider.care_provider_mname, careProvider.care_provider_lname,
  technician,
  laboratory, result, comments, patient_id
  FROM patient_lab
  LEFT OUTER JOIN labDescription
  ON patient_lab.lab_code = labDescription.lab_code
  LEFT OUTER JOIN careProvider
  ON patient_lab.lab_order_by = careProvider.care_provider_id
  WHERE patient_lab.patient_id = ${req.params.id} AND patient_lab.lab_id = ${req.params.labID}`;
  connection.query(sqlSelect, (error, results) => {
    if (error) throw error;
    return res.status(200).send(results);
  });
});



// Insert lab
router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO patient_lab (lab_date, laboratory, lab_code, technician, result, comments, patient_id, lab_order_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
    [req.body.lab_date, 
      req.body.laboratory, 
      req.body.lab_code,
      req.body.technician, 
      req.body.result, 
      req.body.comments, 
      req.body.patient_id, 
      req.body.lab_order_by],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});


 // Update lab 
 router.put("/:id", (req, res) => {
  const { 
    lab_date, 
    laboratory, 
    lab_code, 
    technician, 
    result, 
    comments, 
    lab_order_by
  } = req.body;
  const sqlUpdate = 
  `UPDATE patient_lab
  SET lab_date = "${lab_date}",
  laboratory = "${laboratory}",
  lab_code = "${lab_code}",
  technician = "${technician}",
  result = "${result}",
  comments = "${comments}",
  lab_order_by = "${lab_order_by}"
  WHERE patient_lab.lab_id = ${req.params.id}`;

  connection.query(sqlUpdate,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


// Delete lab 
router.delete("/:id", (req, res) => {
  connection.query(
    `DELETE FROM patient_lab WHERE lab_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

export default router;
