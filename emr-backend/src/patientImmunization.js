import express from "express";

const router = express.Router();

router.post("/", (req, res) => {
  connection.query(
    "INSERT INTO patient_immunization (immunization_name, dose, date_completed, comments, administer_by, patient_id, care_provider_id) VALUES  (?, ?, ?, ?, ?, ?, ?)",
    [
      req.body.immuName,
      req.body.dose,
      req.body.date,
      req.body.comment,
      req.body.administrator,
      req.body.patientId,
      req.body.careProviderId
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

router.delete("/:id", (req, res) => {
  connection.query(
    `DELETE FROM patient_immunization WHERE immunization_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

router.put("/:id", (req, res) => {
  const { immunization_name, dose, date_completed, comments, administer_by } = req.body;
  connection.query(
    `UPDATE patient_immunization SET immunization_name="${immunization_name}", dose="${dose}", date_completed="${date_completed}", comments="${comments}", administer_by="${administer_by}"  WHERE immunization_id=${req.params.id}`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );

});

router.get("/:id", (req, res) => {
  connection.query(
    `SELECT patient_immunization.immunization_id, patient_immunization.immunization_name, patient_immunization.dose, SUBSTRING_INDEX(patient_immunization.date_completed, "T", 1) as date_completed , patient_immunization.comments,patient_immunization.administer_by, CONCAT(careProvider.care_provider_fname, ' ', careProvider.care_provider_mname, ' ', careProvider.care_provider_lname) as careProvider_name, CONCAT(patient.first_name, patient.middle_name, patient.last_name) as patient_name FROM patient_immunization INNER JOIN careProvider ON patient_immunization.care_provider_id = careProvider.care_provider_id INNER JOIN patient ON patient_immunization.patient_id = patient.patient_id WHERE patient.patient_id = ${req.params.id};`,
    function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

export default router;