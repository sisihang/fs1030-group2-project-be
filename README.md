# FS1030-S21-Group2-EMR Project (Backend Repo)

**Description:**  This is an Electronic Medical Record Management System designed to be used by a care provider to be able to pull/update patient's medical information.

    Note: This repo is the Backend portion of the project. The Fronend and Database repos are needed to be able to see the complete project package.

        Front End Repo Link: https://gitlab.com/sisihang/fs1030-group2-project-fe

        Database Repo Link: https://gitlab.com/nols2021/fs1030-s21-group2-emr-database/

**Technologies**

    The project has been developed using REACT, NodeJS and MySQL.
    Git Bash Terminal / Visual Studio Code / MySQL Workbench can be used to clone the project / work with the database.

**Getting started**

    Step1: Project Repo Cloning Process
        -Open bash terminal then create a folder where to store the repo
        -Change directory to the newly created folder
        -Clone the repo
        -At GitLab >> Go to Repository >> Select the 'main' repo from the dropdown
        -On the right side >> Right click on 'Clone' dropdown
        -Select Clone with HTTPS
        -Go back to the terminal then issue the command below (without the '<' and '>' symbols)
            git clone 
        -Change directory to the repo folder "emr-backend"

    Step2: Environment Variable Setup
        -At the repo folder, open Visual Studio Code
        -Rename .envCopy to .env file or create .env file with following Port and JWT_SECRET value 
            -PORT=4000
            -JWT_SECRET=test

    Step3: Install Dependencies
        -Still at Visual Studio Code, open the terminal (Go to Terminal Menu >> New Terminal)
        -Type the command below then hit enter key

                npm install

    Step4: Update database password to your local mysql database host, user and password in index.js
        app.use(function(req, res, next){
        global.connection = mysql.createConnection({
        host     : 'localhost',
        user     : 'root',
        password : '@Theboys4',
        database : 'emr',
        dateStrings: 'date'
        });


    Step5: Start Backend Application (Development Mode)
        -Type the command below then hit enter key

                npm run dev


        -Open http://localhost:3000 to view it in the browser.

            Note: You should see the message "Express Server started at http://localhost:4000
                  

**SQL Statements**
**Note: The succeeding files are under 'src' folder**
    

**File Name: patientAllergy.js

    Line 7
    INSERT INTO patient_medication (medication_name, instruction, patient_id, care_provider_id) VALUES  (?, ?, ?, ?)

    Line 23
    DELETE FROM patient_medication WHERE medication_id=${req.params.id}

    Line 34
    UPDATE patient_medication SET medication_name="${medication_name}", instruction="${instruction}" WHERE medication_id=${req.params.id}

    line 44
    SELECT medication_id, medication_name, instruction from patient_medication WHERE patient_id= ${req.params.id};
    patientImmunization

    Line 7
    INSERT INTO patient_immunization (immunization_name, dose, date_completed, comments, administer_by, patient_id, care_provider_id) VALUES  (?, ?, ?, ?, ?, ?, ?)

    Line 26
    DELETE FROM patient_immunization WHERE immunization_id=${req.params.id}

    Line 37
    UPDATE patient_immunization SET immunization_name="${immunization_name}", dose="${dose}", date_completed="${date_completed}", comments="${comments}",   administer_by="${administer_by}"  WHERE immunization_id=${req.params.id}

    Line 48
    SELECT patient_immunization.immunization_id, patient_immunization.immunization_name, patient_immunization.dose, SUBSTRING_INDEX(patient_immunization.date_completed, "T", 1) as date_completed , patient_immunization.comments,patient_immunization.administer_by, CONCAT(careProvider.care_provider_fname, ' ', careProvider.care_provider_mname, ' ', careProvider.care_provider_lname) as careProvider_name, CONCAT(patient.first_name, patient.middle_name, patient.last_name) as patient_name FROM patient_immunization INNER JOIN careProvider ON patient_immunization.care_provider_id = careProvider.care_provider_id INNER JOIN patient ON patient_immunization.patient_id = patient.patient_id WHERE patient.patient_id = ${req.params.id}
    
**File Name: patientMedication.js

    Line 7
    INSERT INTO patient_medication (medication_name, instruction, patient_id, care_provider_id) VALUES  (?, ?, ?, ?)

    Line 23
    DELETE FROM patient_medication WHERE medication_id=${req.params.id}

    Line 34
    UPDATE patient_medication SET medication_name="${medication_name}", instruction="${instruction}" WHERE medication_id=${req.params.id}

    Line 44
    SELECT medication_id, medication_name, instruction from patient_medication WHERE patient_id= ${req.params.id}

**File Name: revisionHistory.js

    Line 7
    INSERT INTO change_history (column_name, revision_date, patient_id, care_provider_id) VALUES  (?, ?, ?, ?)

    Line 24
    SELECT change_history.column_name, change_history.revision_date,CONCAT(careProvider.care_provider_fname, ' ', careProvider.care_provider_mname, ' ', careProvider.care_provider_lname) as careProvider_name FROM change_history LEFT JOIN careProvider ON change_history.care_provider_id = careProvider.care_provider_id WHERE patient_id = ${req.params.id}

**File Name: medicalHistorty.js

    Line 7:
    "INSERT INTO medical_history ( patient_id, care_provider_id, medical_history_date, major_illness, surgical_procedure, comments) VALUES  (?, ?, ?, ?, ?, ?)",
    [
    req.body.patientId,
    req.body.careProviderId,
    req.body.medHistoryDate,
    req.body.majIllness,
    req.body.surgProcedure,
    req.body.comments,
    ],

    Line 25:
    DELETE FROM medical_history WHERE medical_history_id=${req.params.id},

    Line: 36
    UPDATE medical_history SET medical_history_date="${medical_history_date}", major_illness="${major_illness}" surgical_procedure="${surgical_procedure}", comments="${comments}" WHERE medical_history_id=${req.params.id},

    Line 46:
    "SELECT * FROM medical_history",

    Line 56:
    SELECT medical_history_id, care_provider_id, SUBSTRING_INDEX(medical_history.medical_history_date, "T", 1) as medical_history_date, major_illness, surgical_procedure, comments FROM medical_history WHERE patient_id=${req.params.id},

**File Name: radiology.js

    Line 7:
    "INSERT INTO radiology (image_type, radio_image, patient_id, radio_date, radiographer, care_provider_id, clinic_name, comments) VALUES  (?, ?, ?, ?, ?, ?, ?, ?)",
    [
    req.body.imageType,
    req.body.radioImage,
    req.body.patientId,
    req.body.radioDate,
    req.body.radiographer,
    req.body.careProviderId,
    req.body.clinicName,
    req.body.comments
    ],

    Line 27:
    DELETE FROM radiology WHERE radiograph_id=${req.params.id},

    Line 38:
    UPDATE radiology SET image_type="${image_type}", radio_image="${radio_image}", radio_date="${radio_date}", radiographer="${radiographer}", clinic_name="${clinic_name}", comments="${comments}" WHERE radiograph_id="${req.params.id}",

    Line 48:
    "SELECT * FROM radiology"

    Line 58:
    SELECT radiograph_id, image_type, radio_image, SUBSTRING_INDEX(radiology.radio_date, "T", 1) as radio_date, radiographer, care_provider_id, clinic_name, comments FROM radiology WHERE patient_id=${req.params.id};,

**File Name: billing.js

    Line 7:
    "INSERT INTO billing (care_provider_id, patient_id, service_rendered, service_date, billing_date, due_date, total_amount, payment, balance) VALUES  (?, ?, ?, ?, ?, ?, ?, ?, ?)",
    [
    req.body.careProviderId,
    req.body.patientId,
    req.body.serviceRendered,
    req.body.serviceDate,
    req.body.billingDate,
    req.body.dueDate,
    req.body.totalAmount,
    req.body.payment,
    req.body.balance,
    ],

    Line 30:
    DELETE FROM billing WHERE bill_invoice_id=${req.params.id},

    Line 41:
    UPDATE billing SET service_rendered="${service_rendered}", service_date="${service_date}" billing_date="${billing_date}", due_date="${due_date}", total_amount="${total_amount}", payment="${payment}", balance="${balance}" WHERE bill_invoice_id=${req.params.id},

    Line 51:
    SELECT bill_invoice_id, care_provider_id, service_rendered, SUBSTRING_INDEX(billing.service_date, "T", 1) as service_date, SUBSTRING_INDEX(billing.billing_date, "T", 1) as billing_date,  SUBSTRING_INDEX(billing.due_date, "T", 1) as due_date, total_amount, payment, balance FROM billing WHERE patient_id= ${req.params.id};,

    Line 61:
    "SELECT * FROM billing",

**File Name: patientRouter.js

    Line 7
    SELECT patient.patient_id, healthcard_no, healthcard_ver, first_name, middle_name, last_name, address_line1, address_line2, city, province_code, postal_code, home_phone, cell_phone, email, family_doctor_code, employer_name, work_phone, occupation, dob, gender_code, race_id, marital_status_code, pref_language_id
    FROM patient
    LEFT OUTER JOIN demographic
    ON patient.patient_id = demographic.patient_id

    Line 22
    SELECT patient.patient_id, healthcard_no, healthcard_ver, first_name, middle_name, last_name,
    address_line1, address_line2, city, patient.province_code, province.province_name, postal_code,
    home_phone, cell_phone, patient.email, patient.family_doctor_code,
    demographic.employer_name, demographic.work_phone, demographic.occupation, demographic.dob,
    DATE_FORMAT(FROM_DAYS(DATEDIFF(now(), dob)), '%Y')+0 AS age,
    demographic.gender_code, gender.gender_description,
    demographic.race_id, race.race_name,
    demographic.marital_status_code, marital_status_desc,
    demographic.pref_language_id, preferredLanguage.pref_language_name,
    careProvider.care_provider_fname, careProvider.care_provider_mname, careProvider.care_provider_lname
    FROM patient
    LEFT OUTER JOIN careProvider
    ON patient.family_doctor_code = careProvider.care_provider_id
    LEFT OUTER JOIN province
    ON patient.province_code = province.province_code
    LEFT OUTER JOIN demographic
    ON patient.patient_id = demographic.patient_id
    LEFT OUTER JOIN gender
    ON demographic.gender_code = gender.gender_code
    LEFT OUTER JOIN race
    ON demographic.race_id = race.race_id
    LEFT OUTER JOIN maritalStatus
    ON demographic.marital_status_code = maritalStatus.marital_status_code
    LEFT OUTER JOIN preferredLanguage
    ON demographic.pref_language_id = preferredLanguage.pref_language_id
    WHERE patient.patient_id = ${req.params.id}

    Line 76
    UPDATE patient
    SET first_name = "${first_name}",
    middle_name = "${middle_name}",
    last_name = "${last_name}",
    address_line1 = "${address_line1}",
    address_line2 = "${address_line2}",
    city = "${city}",
    province_code = "${province_code}",
    postal_code = "${postal_code}",
    home_phone = "${home_phone}",
    cell_phone = "${cell_phone}",
    email = "${email}"
    WHERE patient_id = ${req.params.id}`

**File Name: demographicRouter.js

    Line 20
    UPDATE demographic
    SET employer_name = "${employer_name}",
    work_phone = "${work_phone}",
    occupation = "${occupation}",
    dob = "${dob}",
    patient_id = "${patient_id}",
    gender_code = "${gender_code}",
    pref_language_id = "${pref_language_id}",
    race_id = "${race_id}",
    marital_status_code = "${marital_status_code}"
    WHERE demo_id = ${req.params.id}

**File Name: labRouter.js

    Line 9
    SELECT lab_id, lab_date, patient_lab.lab_code, labDescription.lab_desc,
    lab_order_by, patient_lab.lab_order_by, careProvider.care_provider_fname, careProvider.care_provider_mname, careProvider.care_provider_lname,
    technician,
    laboratory, result, comments, patient_id
    FROM patient_lab
    LEFT OUTER JOIN labDescription
    ON patient_lab.lab_code = labDescription.lab_code
    LEFT OUTER JOIN careProvider
    ON patient_lab.lab_order_by = careProvider.care_provider_id
    WHERE patient_lab.patient_id = ${req.params.id}

    Line 30
    SELECT lab_id, lab_date, patient_lab.lab_code, labDescription.lab_desc,
    lab_order_by, patient_lab.lab_order_by, careProvider.care_provider_fname, careProvider.care_provider_mname, careProvider.care_provider_lname,
    technician,
    laboratory, result, comments, patient_id
    FROM patient_lab
    LEFT OUTER JOIN labDescription
    ON patient_lab.lab_code = labDescription.lab_code
    LEFT OUTER JOIN careProvider
    ON patient_lab.lab_order_by = careProvider.care_provider_id
    WHERE patient_lab.patient_id = ${req.params.id} AND patient_lab.lab_id = ${req.params.labID}

    Line 51
    INSERT INTO patient_lab (lab_date, laboratory, lab_code, technician, result, comments, patient_id, lab_order_by) VALUES (?, ?, ?, ?, ?, ?, ?, ?)",
    [req.body.lab_date,
    req.body.laboratory,
    req.body.lab_code,
    req.body.technician,
    req.body.result,
    req.body.comments,
    req.body.patient_id,
    req.body.lab_order_by]

    Line 80
    UPDATE patient_lab
    SET lab_date = "${lab_date}",
    laboratory = "${laboratory}",
    lab_code = "${lab_code}",
    technician = "${technician}",
    result = "${result}",
    comments = "${comments}",
    lab_order_by = "${lab_order_by}"
    WHERE patient_lab.lab_id = ${req.params.id}

    Line 102
    DELETE FROM patient_lab WHERE lab_id=${req.params.id}

**File Name: chartEntriesRouter.js

    Line 8
    SELECT chart_entry_id, chart_entry.patient_id, chart_entry_date, comments,
    chart_entry.care_provider_id, careProvider.care_provider_fname, careProvider.care_provider_mname, careProvider.care_provider_lname, comments
    FROM chart_entry
    LEFT OUTER JOIN careProvider
    ON chart_entry.care_provider_id = careProvider.care_provider_id
    WHERE patient_id = ${req.params.id}
    ORDER BY chart_entry_date DESC

    Line 26
    SELECT chart_entry_id, chart_entry.patient_id, chart_entry_date, comments,
    chart_entry.care_provider_id, careProvider.care_provider_fname, careProvider.care_provider_mname, careProvider.care_provider_lname, comments
    FROM chart_entry
    LEFT OUTER JOIN careProvider
    ON chart_entry.care_provider_id = careProvider.care_provider_id
    WHERE patient_id = ${req.params.id} AND chart_entry_id = ${req.params.noteID}
    ORDER BY chart_entry_date DESC

    Line 45
    INSERT INTO chart_entry (chart_entry_date, comments, care_provider_id, patient_id) VALUES (?, ?, ?, ?)",
    [req.body.chart_entry_date,
    req.body.comments,
    req.body.care_provider_id,
    req.body.patient_id]

    Line 68
    UPDATE chart_entry
    SET chart_entry_date = "${chart_entry_date}",
    comments = "${comments}",
    patient_id = "${patient_id}",
    care_provider_id = "${care_provider_id}"
    WHERE chart_entry_id = "${req.params.id}"

**File Name: patientEmergency.js

    Line 7
    SELECT * FROM patientContact WHERE patient_id = ${req.params.id}

**File Name: patientContact.js

    line 18
    SELECT * FROM patientContact WHERE patient_id = ${req.params.id} AND contact_id = ${req.params.contactID}

    Line 29
    INSERT INTO patientContact (contact_fname, contact_lname, contact_phone, relationship, patient_id) VALUES (?, ?, ?, ?, ?)",
    [req.body.contact_fname,
    req.body.contact_lname,
    req.body.contact_phone,
    req.body.relationship,
    req.body.patient_id]

    Line 53
    UPDATE patientContact
    SET contact_fname = "${contact_fname}",
    contact_lname = "${contact_lname}",
    contact_phone = "${contact_phone}",
    relationship = "${relationship}"
    pateint_id = "${patient_id}"
    WHERE contact_id = ${req.params.id}

    Line 72
    DELETE FROM patientContact WHERE contact_id=${req.params.id}
  

**Note: The succeeding files are under 'src/models' folder**
**File Name: careprovider.model.js

    Line 22
    SELECT * FROM CAREPROVIDER

    Line 35
    SELECT * FROM careprovider WHERE first_name=?

    Line 47
    INSERT INTO careprovider SET ?

    Line 60
    SELECT * FROM careprovider WHERE id=?

    Line 76
    UPDATE careprovider SET first_name=?,middle_name=?,last_name=?,email=?,cspo_number=?,initial_setup=?, careprovider_type_id=? WHERE id = ?

    Line 93
    DELETE FROM careprovider WHERE id=?

    Line 101
    UPDATE careprovider SET is_deleted=? WHERE id = ?


**File Name: patient.model.js

    line 29:
    SELECT * FROM PATIENT

    line 42:
    SELECT * FROM patient WHERE first_name=?

    line 54:
    INSERT INTO patient SET ?

    line 67:
    SELECT * FROM patient WHERE id=?

    line 83:
    UPDATE patient SET healthcard_no=?,healthcard_ver=?,first_name=?,middle_name=?,last_name=?,address_line1=?,address_line2=?,city=?,province_code=?,postal_code=?,home_phone=?,cell_phone=?,email=?,family_doctor_code=?  WHERE id = ?", [patientReqData.healthcard_no, patientReqData.healthcard_ver, patientReqData.first_name, patientReqData.middle_name, patientReqData.last_name, patientReqData.address_line1, patientReqData.address_line2, patientReqData.city, patientReqData.province_code, patientReqData.postal_code, patientReqData.home_phone, patientReqData.cell_phone, patientReqData.email, patientReqData.family_doctor_code, id]

    line 100:
    DELETE FROM patient WHERE id=?
